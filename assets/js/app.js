$(document).ready(function(){

    // Buscar Registros del dia en curso y mostrarlos al cargar la pagina
    $.post("assets/php/Funciones.php",{Action:"obtener_registros"},function(data){
        if(data.length > 0)
        {
            var i = 0;
            var Row = "";

            for(i = 0; i<data.length; i++)
            {
                Row += `
                <tr>
                    <td class="text-center">${i+1}</td>
                    <td>${data[i].Estacion}</td>
                    <td>${data[i].NoEmpleado}</td>
                    <td>${data[i].Empleado}</td>
                    <td class="text-right"> <a href="#">Ver Detalle</a></td>
                    <td class="td-actions text-right">${data[i].Fecha}</td>
                </tr>
                `;
            }

            $("#tbody_acumulados").html(Row);
        }
    },"JSON");

    // Al dar click al boton de Autorizar se guarda el registro y se listan todos los registros para actualizar la tabla
    $("#btn_autorizar").on("click",function(e){
        e.preventDefault();

        var iUsuario = "1";
        var Estacion = "";
        var NoEmpleado = "";
        var Empleado = "";
        var Mouse = "";
        var CPU = "";
        var Diadema = "";
        var Monitor = "";
        var Mampara = "";
        var Teclado = "";
        var SO = "";
        var Observacion = "";

        Estacion = $("#txt_estacion").val();
        NoEmpleado = $("#txt_noempleado").val();
        Empleado = $("#txt_empleado").val();
        Mouse = $("#chk_mouse").prop('checked') ? '1':'0';
        CPU = $("#chk_cpu").prop('checked') ? '1':'0';
        Diadema = $("#chk_diadema").prop('checked') ? '1':'0';
        Monitor = $("#chk_monitor").prop('checked') ? '1':'0';
        Mampara = $("#chk_mampara").prop('checked') ? '1':'0';
        Teclado = $("#chk_teclado").prop('checked') ? '1':'0';
        SO = $("#chk_so").prop('checked') ? '1':'0';
        Observacion = $("#txt_observacion").val();

        $.post("assets/php/Funciones.php",{Action:"agregar_registro",iUsuario:iUsuario,Estacion:Estacion,NoEmpleado:NoEmpleado,Empleado:Empleado,Mouse:Mouse,CPU:CPU,Diadema:Diadema,Monitor:Monitor,Mampara:Mampara,Teclado:Teclado,SO:SO,Observacion:Observacion},function(data){

            if(data.length > 0)
            {
                var i = 0;
                var Row = "";

                for(i = 0; i<data.length; i++)
                {
                    Row += `
                    <tr>
                        <td class="text-center">${i+1}</td>
                        <td>${data[i].Estacion}</td>
                        <td>${data[i].NoEmpleado}</td>
                        <td>${data[i].Empleado}</td>
                        <td class="text-right"> <a href="#">Ver Detalle</a></td>
                        <td class="td-actions text-right">${data[i].Fecha}</td>
                    </tr>
                    `;
                }

                $("#tbody_acumulados").html(Row);
            }
          

        },"JSON");

    });
});