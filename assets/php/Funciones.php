<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
    if(isset($_POST['Action']))
    {
        $Result = "";
        
        $Action = $_POST["Action"];
        
        switch($Action)
        {
            case "agregar_registro": $Result = agregar_registro();break;
            case "obtener_registros": $Result = obtener_registros();break;
        }
        
        echo $Result;
    }

    function agregar_registro()
    {
        $iUsuario = $_POST["iUsuario"];
        $Estacion = $_POST["Estacion"];
        $NoEmpleado = $_POST["NoEmpleado"];
        $Empleado = $_POST["Empleado"];
        $Mouse = $_POST["Mouse"];
        $CPU = $_POST["CPU"];
        $Diadema = $_POST["Diadema"];
        $Monitor = $_POST["Monitor"];
        $Mampara = $_POST["Mampara"];
        $Teclado = $_POST["Teclado"];
        $SO = $_POST["SO"];
        $Observacion = $_POST["Observacion"];

        $Query = "INSERT INTO diario (iUsuario,Estacion,NoEmpleado,Empleado,Mouse,CPU,Diadema,Monitor,Mampara,Teclado,SO,Observacion) VALUES ('".$iUsuario."','".$Estacion."','".$NoEmpleado."','".$Empleado."','".$Mouse."','".$CPU."','".$Diadema."','".$Monitor."','".$Mampara."','".$Teclado."','".$SO."','".$Observacion."')";

        Ejecuta_Query($Query);
        
        return obtener_registros();
    }

    function obtener_registros()
    {
        $Row = array();
        $Query = "SELECT * FROM diario WHERE DATE(Fecha) = DATE(NOW()) ORDER BY iDiario DESC";
        $result = Ejecuta_Query($Query);
        
        while($Registro = $result->fetch_assoc())
        {
            $Row[] = $Registro;
        }
        
        return json_encode($Row);
    }

    function Ejecuta_Query ($Query)
    {
        include("ConnDB.php");
        
        //Abre una conexion a MySQL server
        $mysqli = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);

        //Arrojo cualquier error tipo connection error
        if ($mysqli->connect_error) {
            die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
        }
        $result = $mysqli->query($Query);
        //Cierro la conexion 
        $mysqli->close();
        
        return $result;
    }
?>  